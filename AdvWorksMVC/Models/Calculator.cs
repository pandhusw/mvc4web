﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvWorksMVC.Models
{
    public class Calculator
    {
        public static int perkalian(int x, int y)
        {
            return x * y;
        }

        public static int pertambahan(int x, int y)
        {
            return x + y;
        }

        public static int pembagian(int x, int y)
        {
            return x / y;
        }

    }
}