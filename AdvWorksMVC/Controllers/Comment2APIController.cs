﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AdvWorksMVC.Models;

namespace AdvWorksMVC.Controllers

   
{
    public class Comment2APIController : ApiController
    {
        private AdventureWorks2014Entities1 db = new AdventureWorks2014Entities1();

        // GET api/Comment2API/
        public IEnumerable<Comment> GetComments()
        {

            List<Comment> result = new List<Comment>();
            Comment temp;
            foreach (var emp in db.Comments.ToList())
            {
                temp = new Comment()
                {
                    Subject = emp.Subject,
                    User = emp.User,
                    CommentID = emp.CommentID,
                    PhotoPhotoID = emp.PhotoPhotoID
                };
                result.Add(temp);
            }
            return result;
        }

        // GET api/Comment2API/5
        public Comment GetComment(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return new Comment(){
            PhotoPhotoID = comment.PhotoPhotoID,
            CommentID = comment.CommentID,
            User = comment.User
                //ketika coba ../API/Comment2API/24 dll karna yg id awal2 sudah di delete di db
        };
        }

        // PUT api/Comment2API/5
        public HttpResponseMessage PutComment(int id, Comment comment)
        {
            if (ModelState.IsValid && id == comment.CommentID)
            {
                db.Entry(comment).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Comment2API
        public HttpResponseMessage PostComment(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, comment);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = comment.CommentID }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Comment2API/5
        public HttpResponseMessage DeleteComment(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Comments.Remove(comment);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, comment);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}