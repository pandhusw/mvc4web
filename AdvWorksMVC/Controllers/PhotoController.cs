﻿using AdvWorksMVC.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdvWorksMVC.Controllers
{
    public class PhotoController : Controller
    {
        AdventureWorks2014Entities1 advEntity = new AdventureWorks2014Entities1();

        private static Logger log = LogManager.GetCurrentClassLogger();

        //
        // GET: /Photo/

        public ActionResult Index()
        {
             
            return View(advEntity.Photos.ToList());
        }

        [HttpGet]
        public ActionResult prefix(String id = "")
        {
            var hasil = from x in advEntity.Photos
                        where x.Title.Contains(id) | x.Owner.Contains(id) | x.Description.Contains(id)
                        select x;
            log.Debug(hasil.Count());
            return View("Index",hasil.ToList());
            
        } 
        


        [HttpGet]
        public ActionResult Tambah()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Tambah(Photo photo)
        {

            if (ModelState.IsValid)
            {
                
                //var Photo = new Photo();
                var Comment = new Comment();

                photo.PhotoFile = new byte[64];

                Comment.User = "ini user comment";
                Comment.Subject = "ini subject comment";
                Comment.Body = "ini body";

               // advEntity.Comments.Add(Comment);
              //  advEntity.Photos.Add(photo);
               // advEntity.SaveChanges();

                ViewBag.photo = photo;
            
            return View();
            }
            return View(photo);
        }

        [HttpGet]
        public ActionResult Ubah(short id=0)
        {
            Photo editPhoto =
                advEntity.Photos.Find(id);

            if (editPhoto == null)
                return HttpNotFound();
            return View(editPhoto);
        }

        [HttpPost]
        public ActionResult Ubah(Photo photo)
        {
            if (ModelState.IsValid)
            {
               // advEntity.Entry(photo).State = System.Data.EntityState.Modified;

              //  advEntity.Photos
             //   advEntity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(photo);
        }

        [HttpGet]
        public ActionResult Hapus(short id = 0)
        {
            Photo hapusPhoto =
                advEntity.Photos.Find(id);

            if (hapusPhoto != null)
            {
                List<int> idComment = new List<int>();
                foreach (var comment in hapusPhoto.Comments)
                {
                    idComment.Add(comment.CommentID);
                    
                }

                foreach (var idC in idComment)
                {
                    advEntity.Comments.Remove(advEntity.Comments.Find(idC));
                }

                advEntity.Photos.Remove(hapusPhoto);
                advEntity.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        //
        //[HttpGet]
        //public ActionResult Comment()
        //{

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Comment(Comment comment)
        //{

        //    if (ModelState.IsValid)
        //    {

        //        //var Photo = new Photo();
        //        var Comment = new Comment();

        //        //photo.PhotoFile = new byte[64];

        //        Comment.User = "ini user comment";
        //        Comment.Subject = "ini subject comment";
        //        Comment.Body = "ini body";

        //        // advEntity.Comments.Add(Comment);
        //        //  advEntity.Photos.Add(photo);
        //        // advEntity.SaveChanges();

        //        ViewBag.comment = comment;

        //        return View();
        //    }
        //    return View(comment);
        //}


        

    }
}
