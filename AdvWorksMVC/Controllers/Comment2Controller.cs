﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdvWorksMVC.Models;

namespace AdvWorksMVC.Controllers
{
    public class Comment2Controller : Controller
    {
        private AdventureWorks2014Entities1 db = new AdventureWorks2014Entities1();

        //
       


        // GET: /Comment2/

        public ActionResult Index()
        {
            var comments = db.Comments.Include(c => c.Photo);
            return View(comments.ToList());
        }

        //
        // GET: /Comment2/Details/5

        public ActionResult Details(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /Comment2/Create

        public ActionResult Create()
        {
            ViewBag.PhotoPhotoID = new SelectList(db.Photos, "PhotoID", "Title");
            return View();
        }

        //
        // POST: /Comment2/Create

        [HttpPost]
        public ActionResult Create(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PhotoPhotoID = new SelectList(db.Photos, "PhotoID", "Title", comment.PhotoPhotoID);
            return View(comment);
        }

        //
        // GET: /Comment2/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.PhotoPhotoID = new SelectList(db.Photos, "PhotoID", "Title", comment.PhotoPhotoID);
            return View(comment);
        }

        //
        // POST: /Comment2/Edit/5

        [HttpPost]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PhotoPhotoID = new SelectList(db.Photos, "PhotoID", "Title", comment.PhotoPhotoID);
            return View(comment);
        }

        //
        // GET: /Comment2/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment2/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}