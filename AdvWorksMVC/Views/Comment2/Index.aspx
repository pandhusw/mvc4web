﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AdvWorksMVC.Models.Comment>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>

<p>
    <%: Html.ActionLink("Create New", "Create") %>
</p>
<table>
    <tr>
        <th>
            <%: Html.DisplayNameFor(model => model.User) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Subject) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Body) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Photo.Title) %>
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.User) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Subject) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Body) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Photo.Title) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { id=item.CommentID }) %> |
            <%: Html.ActionLink("Details", "Details", new { id=item.CommentID }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { id=item.CommentID }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
