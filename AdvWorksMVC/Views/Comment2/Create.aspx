﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<AdvWorksMVC.Models.Comment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>Comment</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.User) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.User) %>
            <%: Html.ValidationMessageFor(model => model.User) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Subject) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Subject) %>
            <%: Html.ValidationMessageFor(model => model.Subject) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Body) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Body) %>
            <%: Html.ValidationMessageFor(model => model.Body) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PhotoPhotoID, "Photo") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("PhotoPhotoID", String.Empty) %>
            <input type="text" id="PhotoID" />
            <input type="text" id="tags" />
            <%: Html.ValidationMessageFor(model => model.PhotoPhotoID) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>\
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    
    <script>
        $("#PhotoID").autocomplete({
            minLength : 1,
            source : Photo,
            select : function (e, ui) 
            {
                   e.preventDefault();
                   $(this).val(ui.item.label);
                   $("#PhotoID").val(ui.item.value);
            }  
        });
    </script>

    <script>
        $(function () {
            var availableTags = [
              "ActionScript",
              "AppleScript",
              "Asp",
              "BASIC",
              "C",
              "C++",
              "Clojure",
              "COBOL",
              "ColdFusion",
              "Erlang",
              "Fortran",
              "Groovy",
              "Haskell",
              "Java",
              "JavaScript",
              "Lisp",
              "Perl",
              "PHP",
              "Python",
              "Ruby",
              "Scala",
              "Scheme"
            ];
            $("#tags").autocomplete({
                source: availableTags
            });
        });
  </script>
</asp:Content>
