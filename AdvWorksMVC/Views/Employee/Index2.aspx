﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AdvWorksMVC.Models.Employee>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>

<p>
    <%: Html.ActionLink("Create New", "Create") %>
</p>
<table>
    <tr>
        <th>
            <%: Html.DisplayNameFor(model => model.BusinessEntityID) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.NationalIDNumber) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.LoginID) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.OrganizationLevel) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.JobTitle) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.BirthDate) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.MaritalStatus) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.Gender) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.HireDate) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.SalariedFlag) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.VacationHours) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.SickLeaveHours) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.CurrentFlag) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.rowguid) %>
        </th>
        <th>
            <%: Html.DisplayNameFor(model => model.ModifiedDate) %>
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.BusinessEntityID) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.NationalIDNumber) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.LoginID) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.OrganizationLevel) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.JobTitle) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.BirthDate) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.MaritalStatus) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Gender) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.HireDate) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.SalariedFlag) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.VacationHours) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.SickLeaveHours) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CurrentFlag) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.rowguid) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ModifiedDate) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
            <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
