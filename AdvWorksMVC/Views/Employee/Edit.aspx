﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<AdvWorksMVC.Models.Employee>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Edit</h2>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>

    <fieldset>
        <legend>Employee</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.BusinessEntityID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.BusinessEntityID) %>
            <%: Html.ValidationMessageFor(model => model.BusinessEntityID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NationalIDNumber) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NationalIDNumber) %>
            <%: Html.ValidationMessageFor(model => model.NationalIDNumber) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.LoginID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.LoginID) %>
            <%: Html.ValidationMessageFor(model => model.LoginID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.OrganizationLevel) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.OrganizationLevel) %>
            <%: Html.ValidationMessageFor(model => model.OrganizationLevel) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.JobTitle) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.JobTitle) %>
            <%: Html.ValidationMessageFor(model => model.JobTitle) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.BirthDate) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.BirthDate) %>
            <%: Html.ValidationMessageFor(model => model.BirthDate) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.MaritalStatus) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.MaritalStatus) %>
            <%: Html.ValidationMessageFor(model => model.MaritalStatus) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Gender) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Gender) %>
            <%: Html.ValidationMessageFor(model => model.Gender) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.HireDate) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.HireDate) %>
            <%: Html.ValidationMessageFor(model => model.HireDate) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.SalariedFlag) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.SalariedFlag) %>
            <%: Html.ValidationMessageFor(model => model.SalariedFlag) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.VacationHours) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.VacationHours) %>
            <%: Html.ValidationMessageFor(model => model.VacationHours) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.SickLeaveHours) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.SickLeaveHours) %>
            <%: Html.ValidationMessageFor(model => model.SickLeaveHours) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CurrentFlag) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CurrentFlag) %>
            <%: Html.ValidationMessageFor(model => model.CurrentFlag) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.rowguid) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.rowguid) %>
            <%: Html.ValidationMessageFor(model => model.rowguid) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ModifiedDate) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ModifiedDate) %>
            <%: Html.ValidationMessageFor(model => model.ModifiedDate) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%: Scripts.Render("~/bundles/jqueryval") %>
</asp:Content>
