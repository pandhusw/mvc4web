﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AdvWorksMVC.Models.Photo>>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>
    <p>
        <%: Html.ActionLink("Create New", "Tambah") %>
    </p>
    <table>
        <tr>
            <th>
                <%: Html.DisplayNameFor(model => model.Title) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Description) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Owner) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.CreatedDate) %>
            </th>
            <th></th>
        </tr>
    
    <% foreach (var item in Model) { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.Title) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Description) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Owner) %>
            </td>
             <td>
                <%: Html.DisplayFor(modelItem => item.CreatedDate) %>
            </td>
            <td>
                <%: Html.ActionLink("Ubah", "Ubah", new { id=item.PhotoID }) %> |
                <%: Html.ActionLink("Details", "Details", new { id=item.PhotoID }) %> |
               <%: Html.ActionLink("Hapus", "Hapus", new { id=item.PhotoID }) %>
            </td>
        </tr>
         <% } %>
        </table>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
</asp:Content>
